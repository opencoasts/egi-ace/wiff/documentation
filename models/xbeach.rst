XBeach (alpha)
==============

Base
----
.. autoclass:: wiff.models.xbeach.stages.XBeachStage
    :undoc-members:

BindSCHISM
----------
.. autoclass:: wiff.models.xbeach.stages.BindSCHISM
    :undoc-members:

Compute
-------
.. autoclass:: wiff.models.xbeach.stages.Compute
    :undoc-members: