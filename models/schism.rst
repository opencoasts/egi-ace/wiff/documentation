SCHISM
======

Alias
-----
.. autoclass:: wiff.models.schism.stages.Prepare
.. autoclass:: wiff.models.schism.stages.PrepareWWM
.. autoclass:: wiff.models.schism.stages.Forcings
.. autoclass:: wiff.models.schism.stages.Configure
.. autoclass:: wiff.models.schism.stages.Compute
.. autoclass:: wiff.models.schism.stages.Combine
.. autoclass:: wiff.models.schism.stages.Cleanup

Base
----
.. autoclass:: wiff.models.schism.stages.base.SCHISMStage
    :undoc-members:

Prepare
-------
.. autoclass:: wiff.models.schism.stages.prepare.Prepare1
    :undoc-members:

Prepare WWM
-----------
.. autoclass:: wiff.models.schism.stages.prepare_wwm.PrepareWWM0
    :undoc-members:

Forcings
--------
.. autoclass:: wiff.models.schism.stages.forcings.Forcings0
    :undoc-members:

Configure
---------
.. autoclass:: wiff.models.schism.stages.configure.Configure0
    :undoc-members:

Compute
-------
.. autoclass:: wiff.models.schism.stages.compute.Compute0
    :undoc-members:

Combine
-------
.. autoclass:: wiff.models.schism.stages.combine.Combine0
    :undoc-members:

Cleanup
-------
.. autoclass:: wiff.models.schism.stages.cleanup.Cleanup0
    :undoc-members:
