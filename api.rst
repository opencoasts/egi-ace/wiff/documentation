.. _api:

Public
######
This documentation covers the public interfaces of the WIFF components.
Public means they are meant to be used by the end user.
The following components take care of different aspects of a forecast system:

- `Engine`_
- `Forcings`_
- `Submission Proxy`_


Engine
++++++
The engine component provide the base machinery for handing the execution of
forecast systems.
It is composed of several packages:

- `Parts`_
- `Mixins`_
- `Utilities`_

Parts
=====
This package provides classes implementing the base machinery to handle the
execution of a forecast system.
These are model agnostic high level classes meant to help structure systems into
self contained and reusable pieces.

.. image:: images/nested_parts.svg
    :alt: Nested parts representation
    :height: 250px
    :align: right

There are several kinds of classes:

- `Series`_
- `Simulation`_
- `Stage`_

These can be arranged in a nested relation to provide a complete solution, like
is shown in the figure. Series includes simulations and these include stages.

For convenience the following alias are defined:

.. autoclass:: wiff.engine.parts.Series
.. autoclass:: wiff.engine.parts.Simulation
.. autoclass:: wiff.engine.parts.Stage
.. autoclass:: wiff.engine.parts.Layer

Series
------
.. autoclass:: wiff.engine.parts.series.Series1

Simulation
----------
.. autoclass:: wiff.engine.parts.simulation.Simulation1

Stage
-----
.. autoclass:: wiff.engine.parts.stage.Stage1

.. autoclass:: wiff.engine.parts.stage.Layer1

Mixins
======
These classes are intended to be combined with :class:`engine.parts` ones,
as a way of providing them extra capabilities.
The following modules are available:

- `FileHandler`_
- `ProcessHandler`_

FileHandler
-----------
.. autoclass:: wiff.engine.mixins.file_handler.FileHandler
    :no-show-inheritance:

ProcessHandler
--------------
.. autoclass:: wiff.engine.mixins.process_handler.ProcessHandler
    :no-show-inheritance:
    :private-members: _execute

Utilities
=========
Common code used in Engine and Mixins.

QueuedExecution
---------------
.. autoclass:: wiff.core.utils.QueuedExecution
    :no-show-inheritance:


Forcings
++++++++
Not yet public.


Submission Proxy
++++++++++++++++
Not yet public.

--------------------------------------------------------------------------------

Internal
########
This documentation covers the internal interfaces used by WIFF components.
Internal here means they are not intended to be used by the end user, but
instead to be integrated into the WIFF components.
They implement (mostly) common features used in high-level interfaces.

Traits
++++++
.. autoclass:: wiff.core.traits.Identification
    :no-show-inheritance:

.. autoclass:: wiff.core.traits.Logging
    :no-show-inheritance:

.. autoclass:: wiff.core.traits.Registry
    :no-show-inheritance:

Bases
+++++
.. autoclass:: wiff.engine.parts.base.Part0
