Water Information Forcasting Framework
######################################

**WIFF** is a set of python components to help build forecast systems.

The framework is split into several packages:

*engine*
    Provides generic base machinery to handle the execution of forecast systems.

*forcings*
    Implements the means to fetch and convert external forcing data to specific
    shapes and formats.

*sproxy*
    Manages the offloading of the simulation models' execution.

**WIFF runs on python 3.6.**

--------------------------------------------------------------------------------

API Reference
+++++++++++++
WIFF's building blocks.

.. toctree::
    :maxdepth: 3
    :caption: Interfaces:

    api

Model specific stages
+++++++++++++++++++++
WIFF includes ready-to-use stages, specific to a few models.

.. toctree::
    :maxdepth: 2
    :caption: Model stages:

    models/schism
    models/ww3
    models/xbeach

--------------------------------------------------------------------------------

Indices and tables
##################

* :ref:`genindex`
* :ref:`search`

.. * :ref:`modindex`
